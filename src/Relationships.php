<?php

namespace jf\JsonApi;

/**
 * The value of the relationships key MUST be an object (a `relationships object`).
 *
 * Members of the relationships object (`relationships`) represent references from the
 * resource object in which it’s defined to other resource objects.
 *
 * Relationships may be to-one or to-many.
 *
 * @package jfJsonApi
 */
class Relationships extends Meta
{
    /**
     * Rules to validate value of instance.
     *
     * @inheritdoc
     */
    public const RULES = [
        'items' => 'Relationship'
    ];

    /**
     * Adds a new relationship to the list.
     *
     * @param string             $name         Name of relationship.
     * @param Relationship|array $relationship Relationship to add.
     */
    public function addRelationShip(string $name, Relationship|array $relationship)
    {
        $this->setProperties(
            [
                $name => is_array($relationship)
                    ? new Relationship($relationship)
                    : $relationship
            ]
        );
    }
}