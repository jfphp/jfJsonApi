<?php

namespace jf\JsonApi;

use Exception;

/**
 * Exception thrown by default by the validator.
 *
 * @package jfJsonApi
 */
class ValidationException extends Exception
{
}