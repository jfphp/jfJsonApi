<?php

namespace jf\JsonApi;

/**
 * An object containing references to the source of the error.
 *
 * @package jfJsonApi
 */
class ErrorSource extends ANode
{
    /**
     * A string indicating which URI query parameter caused the error.
     *
     * @var string|NULL
     */
    public ?string $parameter = NULL;

    /**
     * A JSON Pointer (RFC6901) to the associated entity in the request document [e.g. "/data"
     * for a primary data object, or "/data/attributes/title" for a specific attribute].
     *
     * @var string|NULL
     */
    public ?string $pointer = NULL;
}