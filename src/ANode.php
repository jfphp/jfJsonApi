<?php

namespace jf\JsonApi;

use JsonSerializable;

/**
 * Base class for document nodes.
 *
 * @package jfJsonApi
 */
abstract class ANode extends ABase implements JsonSerializable
{
    /**
     * Rules to validate value of instance.
     *
     * @var array
     */
    public const RULES = [
        'collide'   => [],
        'dependsOn' => [],
        'items'     => [],
        'oneOf'     => [],
        'required'  => []
    ];

    /**
     * Indicates whether `NULL` values are added to serialization.
     *
     * @var bool
     */
    protected bool $_serializeNullValues = FALSE;

    /**
     * Class constructor.
     *
     * @param array|NULL $values Values to assign to the class properties.
     */
    public function __construct(?array $values = NULL)
    {
        if ($values)
        {
            $this->setProperties($values);
        }
    }

    /**
     * @throws ValidationException
     *
     * @inheritdoc
     */
    public function __serialize() : array
    {
        return $this->toArray();
    }

    /**
     * @inheritdoc
     */
    public function __unserialize(array $data) : void
    {
        if ($data)
        {
            $this->setProperties($data);
        }
    }

    /**
     * Returns the properties of the class that are serializable.
     *
     * @return array
     */
    protected function _getSerializableProperties() : array
    {
        $_properties = [];
        foreach (get_object_vars($this) as $_prop => $_value)
        {
            if ($_prop[0] !== '_')
            {
                $_properties[ $_prop ] = $_value;
            }
        }

        return $_properties;
    }

    /**
     * @throws ValidationException
     *
     * @inheritdoc
     */
    public function jsonSerialize() : ?array
    {
        $_data = $this->toArray();

        return !$_data && !$this->_serializeNullValues
            ? NULL
            : $_data;
    }

    /**
     * Serialize the data.
     *
     * @param array $data Datos a serializar.
     *
     * @return array
     */
    protected function _serializeData(array $data) : array
    {
        $_addNulls = $this->_serializeNullValues;
        foreach ($data as $_key => $_value)
        {
            if ($_value instanceof JsonSerializable)
            {
                $_value = $_value->jsonSerialize();
            }
            if ($_addNulls || $_value !== NULL)
            {
                $data[ $_key ] = is_array($_value)
                    ? $this->_serializeData($_value)
                    : $_value;
            }
            else
            {
                unset($data[ $_key ]);
            }
        }
        ksort($data);

        return $data;
    }

    /**
     * Assign values to properties.
     *
     * @param array|NULL $properties Values to assign.
     *
     * @return array Unused properties.
     */
    public function setProperties(?array $properties = NULL) : array
    {
        $_unused = [];
        if ($properties)
        {
            foreach ($properties as $_property => $_value)
            {
                $_used = FALSE;
                if ($_property[0] !== '_')
                {
                    $_getter = 'get' . ucfirst($_property);
                    if (is_array($_value) && method_exists($this, $_getter))
                    {
                        $_obj = $this->$_getter();
                        if ($_obj instanceof ANode)
                        {
                            $_obj->setProperties($_value);
                            $_used = TRUE;
                        }
                    }
                    if (!$_used && property_exists($this, $_property))
                    {
                        $this->$_property = $_value;
                        $_used = TRUE;
                    }
                }
                if (!$_used)
                {
                    $_unused[ $_property ] = $_value;
                }
            }
        }

        return $_unused;
    }

    /**
     * Converts instance values to array.
     *
     * @return array
     * @throws ValidationException
     */
    public function toArray() : array
    {
        $_data = $this->_serializeData($this->_getSerializableProperties());
        $this->_validate($_data);

        return $_data;
    }

    /**
     * Check if values are valid.
     *
     * @param array $values Values to validate.
     *
     * @throws ValidationException
     */
    protected function _validate(array $values)
    {
        foreach (static::RULES as $_name => $_config)
        {
            if ($_config)
            {
                switch ($_name)
                {
                    case 'collide':
                        $_count = 0;
                        foreach ($_config as $_key)
                        {
                            Validator::assert(
                                !array_key_exists($_key, $values) || $_count++ === 0,
                                500,
                                '{0} - Only one key of {1} is allowed',
                                static::getClassname(),
                                $_config
                            );
                        }
                        break;
                    case 'dependsOn':
                        foreach ($_config as $_key => $_related)
                        {
                            if (array_key_exists($_key, $values))
                            {
                                foreach ($_related as $_dep)
                                {
                                    Validator::assert(
                                        array_key_exists($_dep, $values),
                                        500,
                                        '{0} - Key {1} depends on {2}',
                                        static::getClassname(),
                                        $_key,
                                        $_dep
                                    );
                                }
                            }
                        }
                        break;
                    case 'oneOf':
                        $_oneOf = FALSE;
                        foreach ($_config as $_key)
                        {
                            if (array_key_exists($_key, $values))
                            {
                                $_oneOf = TRUE;
                                break;
                            }
                        }
                        Validator::assert(
                            $_oneOf,
                            500,
                            '{0} - One key of {1} is required',
                            static::getClassname(),
                            $_config
                        );
                        break;
                    case 'required':
                        foreach ($_config as $_key)
                        {
                            Validator::assert(
                                array_key_exists($_key, $values),
                                500,
                                '{0} - Key {1} is required',
                                static::getClassname(),
                                $_key
                            );
                        }
                        break;
                }
            }
        }
    }
}