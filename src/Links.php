<?php

namespace jf\JsonApi;

/**
 * Where specified, a links member can be used to represent links.
 *
 * The value of each `links` member MUST be an object (a `link object`).
 *
 * A links object containing at least one of the following:
 *
 * - related: A related resource link.
 * - self:    A link for the relationship itself (a `relationship link`).
 *            This link allows the client to directly manipulate the relationship.
 *            For example, removing an author through an article's relationship URL would
 * disconnect
 *            the person from the article without deleting the people resource itself.
 *            When fetched successfully, this link returns the linkage for the related
 * resources as
 *            its primary data.
 *
 * @package jfJsonApi
 */
class Links extends ANode
{
    /**
     * Rules to validate value of instance.
     *
     * @inheritdoc
     */
    public const RULES = [
        'oneOf' => [
            'self',
            'related'
        ]
    ];

    /**
     * Pagination links for the primary data.
     *
     * @var Pagination|NULL
     */
    public ?Pagination $pagination = NULL;

    /**
     * Provides access to resource objects linked in a relationship. When fetched, the related
     * resource object(s) are returned as the response's primary data.
     *
     * @var string|NULL
     */
    public ?string $related = NULL;

    /**
     * Identifies the resource represented by the resource object. This link allows the client to
     * directly manipulate the relationship.
     *
     * @var string|NULL
     */
    public ?string $self = NULL;

    /**
     * Returns value from property `pagination` and initializes it if necessary.
     *
     * @return Pagination
     */
    public function getPagination() : Pagination
    {
        return $this->pagination ?? ($this->pagination = new Pagination());
    }
}