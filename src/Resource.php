<?php

namespace jf\JsonApi;

/**
 * The document's `primary data` is a representation of the resource or collection of
 * resources targeted by a request.
 *
 * Every resource object MUST contain an id member and a type member. The values of the id
 * and type members MUST be strings.
 *
 * Within a given API, each resource object's type and id pair MUST identify a single, unique
 * resource. The set of URIs controlled by a server, or multiple servers acting as one,
 * constitute an API.
 *
 * The type member is used to describe resource objects that share common attributes and
 * relationships.
 *
 * The values of type members MUST adhere to the same constraints as member names.
 *
 * @package jfJsonApi
 */
class Resource extends ResourceIdentifier
{
    /**
     * An attributes object representing some of the resource's data.
     *
     * @var Attributes|NULL
     */
    public ?Attributes $attributes = NULL;

    /**
     * Links of resource.
     *
     * @var Links|NULL
     */
    public ?Links $links = NULL;

    /**
     * A relationships object describing relationships between the resource and other JSON API
     * resources.
     *
     * @var Relationships|NULL
     */
    public ?Relationships $relationships = NULL;

    /**
     * Build a resource from an object implementing IResource interfaz.
     *
     * @param IResource $object Object to use to build the resource.
     *
     * @return static|NULL
     */
    public static function fromObject(IResource $object) : ?static
    {
        return new Resource(
            [
                'attributes' => new Attributes($object->getResourceAttributes()),
                'id'         => $object->getResourceId(),
                'type'       => $object->getResourceType()
            ]
        );
    }

    /**
     * Returns value from property `attributes` and initializes it if necessary.
     *
     * @return Attributes
     */
    public function getAttributes() : Attributes
    {
        return $this->attributes ?? ($this->attributes = new Attributes());
    }

    /**
     * Returns value from property `links` and initializes it if necessary.
     *
     * @return Links
     */
    public function getLinks() : Links
    {
        return $this->links ?? ($this->links = new Links());
    }

    /**
     * Returns value from property `relationships` and initializes it if necessary.
     *
     * @return Relationships
     */
    public function getRelationships() : Relationships
    {
        return $this->relationships ?? ($this->relationships = new Relationships());
    }
}