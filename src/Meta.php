<?php

namespace jf\JsonApi;

use JsonSerializable;

/**
 * Where specified, a meta member can be used to include non-standard meta-information.
 *
 * The value of each meta member MUST be an object (a `meta object`).
 *
 * Any members MAY be specified within meta objects.
 *
 * @package jfJsonApi
 */
class Meta extends ANode
{
    /**
     * Contains non-standard meta-information of any type.
     *
     * @var array|NULL
     */
    private ?array $_data = NULL;

    /**
     * Builds an instance from an array.
     *
     * @param array $array Array used to initialize metadata.
     *
     * @return static
     */
    public static function fromArray(array $array) : static
    {
        $_meta        = new static();
        $_meta->_data = $array;

        return $_meta;
    }

    /**
     * Builds an instance from other object.
     *
     * @param JsonSerializable $object Object used to initialize metadata.
     *
     * @return static
     */
    public static function fromJsonSerializable(JsonSerializable $object) : static
    {
        return static::fromArray($object->jsonSerialize());
    }

    /**
     * Builds an instance from other object.
     *
     * @param object $object Object used to initialize metadata.
     *
     * @return static
     */
    public static function fromObject(object $object) : static
    {
        return static::fromArray(get_object_vars($object));
    }

    /**
     * @inheritdoc
     */
    public function jsonSerialize() : ?array
    {
        $_data = $this->_data;

        return $_data
            ? $this->_serializeData($_data)
            : NULL;
    }

    /**
     * @inheritdoc
     */
    public function setProperties(?array $properties = NULL) : array
    {
        if ($properties)
        {
            if ($this->_data === NULL)
            {
                $this->_data = [];
            }
            foreach ($properties as $_property => $_value)
            {
                $this->_data[$_property] = $_value;
            }
        }

        return [];
    }

    /**
     * @inheritdoc
     */
    protected function _validate(array $values)
    {
        parent::_validate($values);
        $_type = static::RULES['items'] ?? NULL;
        if ($_type)
        {
            foreach ($this->_data as $_item)
            {
                Validator::assert($_item instanceof $_type, 400, 'Wrong type');
            }
        }
    }
}