<?php

namespace jf\JsonApi;

use Throwable;

/**
 * A JSON object MUST be at the root of every JSON API request and response containing data.
 *
 * This object defines a document's `top level`.
 *
 * @package jfJsonApi
 */
class Root extends ANode
{
    /**
     * `Content-Type` header to use with JSON API.
     *
     * @var string
     */
    public const CONTENT_TYPE = 'application/vnd.api+json';

    /**
     * @inheritdoc
     */
    public const RULES = [
        'collide'   => [
            'data',
            'errors'
        ],
        'dependsOn' => [
            'included' => [
                'data'
            ]
        ],
        'oneOf'     => [
            'data',
            'errors',
            'meta'
        ]
    ];

    /**
     * The document's primary data.
     *
     * @var Resource|Resource[]|NULL
     */
    private Resource|array|NULL $_data = NULL;

    /**
     * Array of error objects.
     *
     * @var Error[]|NULL
     */
    private ?array $_errors = NULL;

    /**
     * An array of resource objects that are related to the primary data and/or each other.
     *
     * @var Resource[]|NULL
     */
    private ?array $_included = NULL;

    /**
     * An object describing the server's implementation.
     *
     * @var JsonApi|NULL
     */
    public ?JsonApi $jsonapi = NULL;

    /**
     * A links object related to the primary data.
     *
     * @var Links|NULL
     */
    public ?Links $links = NULL;

    /**
     * The document's metadata.
     *
     * @var Meta|NULL
     */
    public ?Meta $meta = NULL;

    /**
     * Adds a resource to result.
     *
     * @param IResource|Resource|array $resource Resource to add.
     *
     * @return Resource|array
     */
    public function addData(IResource|Resource|array $resource) : Resource|array
    {
        return $this->_addToList($resource, $this->_data);
    }

    /**
     * Adds an error to result.
     *
     * @param Error|Throwable $error Error to add.
     */
    public function addError(Error|Throwable $error)
    {
        $this->_errors[] = $error instanceof Error
            ? $error
            : Error::fromThrowable($error);
    }

    /**
     * Adds a resource to included items.
     *
     * @param IResource|Resource|array $resource Resource to add.
     *
     * @return Resource
     */
    public function addIncluded(IResource|Resource|array $resource) : Resource
    {
        return $this->_addToList($resource, $this->_included);
    }

    /**
     * Adds the resource to the list.
     *
     * @param IResource|Resource|array $resource Resource to check.
     * @param mixed                    $list     List to which the resource will be added.
     *
     * @return Resource|Resource[]
     */
    private function _addToList(IResource|Resource|array $resource, mixed &$list = NULL) : Resource|array
    {
        if (!is_array($list))
        {
            $list = [];
        }
        if ($resource)
        {
            if (is_array($resource) && array_is_list($resource))
            {
                foreach ($resource as $index => $item)
                {
                    $resource[ $index ] = $this->_addToList($item, $list);
                }
            }
            else
            {
                $resource      = static::_checkAndBuildResource($resource);
                $_key          = static::buildResourceKey($resource);
                $list[ $_key ] = $resource;
                if (is_array($this->_data) && isset($this->_data[ $_key ]))
                {
                    unset($this->_included[ $_key ]);
                }
            }
        }

        return $resource;
    }

    /**
     * Builds a unique resource key.
     *
     * @param Resource $resource Resource to check.
     *
     * @return string
     */
    public static function buildResourceKey(Resource $resource) : string
    {
        return sprintf('%s-%016s', strtolower($resource->type), $resource->id);
    }

    /**
     * Builds a resource from IResource instance if necessary.
     *
     * @param IResource|Resource|array $resource Resource to check.
     *
     * @return Resource
     */
    private static function _checkAndBuildResource(IResource|Resource|array $resource) : Resource
    {
        return $resource instanceof Resource
            ? $resource
            : (is_array($resource) ? new Resource($resource) : Resource::fromObject($resource));
    }

    /**
     * Returns value from property `_data`.
     *
     * @return Resource|Resource[]|NULL
     */
    public function getData() : Resource|array|NULL
    {
        return $this->_data;
    }

    /**
     * Returns value from property `_errors`.
     *
     * @return Error[]|NULL
     */
    public function getErrors() : ?array
    {
        return $this->_errors;
    }

    /**
     * Returns value from property `_included`.
     *
     * @return Resource[]|NULL
     */
    public function getIncluded() : ?array
    {
        return $this->_included;
    }

    /**
     * Returns value from property `jsonapi` and initializes it if necessary.
     *
     * @return JsonApi
     */
    public function getJsonapi() : JsonApi
    {
        return $this->jsonapi ?? ($this->jsonapi = new JsonApi());
    }

    /**
     * Returns value from property `links` and initializes it if necessary.
     *
     * @return Links
     */
    public function getLinks() : Links
    {
        return $this->links ?? ($this->links = new Links());
    }

    /**
     * Returns value from property `meta` and initializes it if necessary.
     *
     * @return Meta
     */
    public function getMeta() : Meta
    {
        return $this->meta ?? ($this->meta = new Meta());
    }

    /**
     * @inheritdoc
     */
    protected function _getSerializableProperties() : array
    {
        $_props = parent::_getSerializableProperties();
        if ($this->_errors)
        {
            $_props['errors'] = $this->_errors;
        }
        else
        {
            $_data= $this->_data;
            if ($_data!== NULL)
            {
                $_props['data'] = $_data && is_array($_data)
                    ? array_values($_data)
                    : $_data;
                if ($_data && $this->_included)
                {
                    $_props['included'] = array_values($this->_included);
                }
            }
        }

        return $_props;
    }

    /**
     * Set a resource as result.
     *
     * @param IResource|Resource|array $resource Resource to set.
     */
    public function setData(IResource|Resource|array $resource)
    {
        $this->_data = static::_checkAndBuildResource($resource);
    }

    /**
     * @inheritdoc
     */
    public function setProperties(?array $properties = NULL) : array
    {
        $_unused = parent::setProperties($properties);
        $_data   = $_unused['data'] ?? NULL;
        if ($_data)
        {
            $this->setData($_data);
            unset($_unused['data']);
        }
        $_included = $_unused['included'] ?? NULL;
        if ($_included)
        {
            $this->addIncluded($_included);
            unset($_unused['included']);
        }

        return $_unused;
    }

    /**
     * @inheritdoc
     */
    public function toArray() : array
    {
        $this->getJsonApi();

        return parent::toArray();
    }
}