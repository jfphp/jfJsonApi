<?php

namespace jf\JsonApi;

use Throwable;

/**
 * A server MAY choose to stop processing as soon as a problem is encountered, or it MAY
 * continue processing and encounter multiple problems. For instance, a server might process
 * multiple attributes and then return multiple validation problems in a single response.
 *
 * When a server encounters multiple problems for a single request, the most generally
 * applicable HTTP error code SHOULD be used in the response. For instance, 400 Bad Request
 * might be appropriate for multiple 4xx errors or 500 Internal Server Error might be
 * appropriate for multiple 5xx errors.
 *
 * @package jfJsonApi
 */
class Error extends ANode
{
    /**
     * An application-specific error code, expressed as a string value.
     *
     * @var string|NULL
     */
    public ?string $code = NULL;

    /**
     * A human-readable explanation specific to this occurrence of the problem. Like `title`,
     * this field's value can be localized.
     *
     * @var string|NULL
     */
    public ?string $detail = NULL;

    /**
     * Unique identifier for this particular occurrence of the problem.
     *
     * @var string|NULL
     */
    public ?string $id = NULL;

    /**
     * A links object containing error references.
     *
     * @var ErrorLink|NULL
     */
    public ?ErrorLink $links = NULL;

    /**
     * A meta object containing non-standard meta-information about the error.
     *
     * @var Meta|NULL
     */
    public ?Meta $meta = NULL;

    /**
     * An object containing references to the source of the error.
     *
     * @var ErrorSource|NULL
     */
    public ?ErrorSource $source = NULL;

    /**
     * The HTTP status code applicable to this problem, expressed as a string value.
     *
     * @var string|NULL
     */
    public ?string $status = NULL;

    /**
     * A short, human-readable summary of the problem that SHOULD NOT change from occurrence to
     * occurrence of the problem, except for purposes of localization.
     *
     * @var string|NULL
     */
    public ?string $title = NULL;

    /**
     * Builds an instance from throwable instance.
     *
     * @param Throwable $error Exception to use for building instance.
     *
     * @return static
     */
    public static function fromThrowable(Throwable $error) : static
    {
        $_traces = $error->getTrace();
        $_trace  = $_traces[0];
        $_config = [
            'code'   => sprintf('%s_%d', mb_convert_case(basename($_trace['file'], '.php'), MB_CASE_UPPER), $_trace['line']),
            'status' => (string) $error->getCode() ?: 400,
            'title'  => $error->getMessage()
        ];
        if (getenv('PHP_ENV') === 'dev')
        {
            if (array_is_list($_traces))
            {
                foreach ($_traces as &$_values)
                {
                    unset($_values['args']);
                }
            }
            $_config['meta'] = $_traces;
        }

        return new static($_config);
    }

    /**
     * Returns value from property `links` and initializes it if necessary.
     *
     * @return ErrorLink
     */
    public function getLinks() : ErrorLink
    {
        return $this->links ?? ($this->links = new ErrorLink());
    }

    /**
     * Returns value from property `meta` and initializes it if necessary.
     *
     * @return Meta
     */
    public function getMeta() : Meta
    {
        return $this->meta ?? ($this->meta = new Meta());
    }

    /**
     * Returns value from property `source` and initializes it if necessary.
     *
     * @return ErrorSource
     */
    public function getSource() : ErrorSource
    {
        return $this->source ?? ($this->source = new ErrorSource());
    }
}