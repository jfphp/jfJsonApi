<?php

namespace jf\JsonApi;

/**
 * Pagination links for the primary data.
 *
 * @package jfJsonApi
 */
class Pagination extends ANode
{
    /**
     * The first page of data.
     *
     * @var string|NULL
     */
    public ?string $first = NULL;

    /**
     * The last page of data.
     *
     * @var string|NULL
     */
    public ?string $last = NULL;

    /**
     * The next page of data.
     *
     * @var string|NULL
     */
    public ?string $next = NULL;

    /**
     * The previous page of data.
     *
     * @var string|NULL
     */
    public ?string $prev = NULL;
}