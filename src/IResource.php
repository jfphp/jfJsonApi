<?php

namespace jf\JsonApi;

use JsonSerializable;

/**
 * Interface for classes used as resources.
 *
 * @package jfJsonApi
 */
interface IResource extends JsonSerializable
{
    /**
     * Returns resource attributes.
     *
     * @return array
     */
    public function getResourceAttributes() : array;

    /**
     * Returns resource identifier.
     *
     * @return string
     */
    public function getResourceId() : string;

    /**
     * Returns resource type.
     *
     * @return string
     */
    public function getResourceType() : string;
}