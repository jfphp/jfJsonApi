<?php

namespace jf\JsonApi;

/**
 * A JSON API document MAY include information about its implementation under a top level
 * `jsonapi` member.
 *
 * @package jfJsonApi
 */
class JsonApi extends ANode
{
    /**
     * Rules to validate value of instance.
     *
     * @inheritdoc
     */
    public const RULES = [
        'required' => [
            'version'
        ]
    ];

    /**
     * This value is a meta object that contains non-standard meta-information.
     *
     * @var Meta|NULL
     */
    public ?Meta $meta = NULL;

    /**
     * This value is a string indicating the highest JSON API version supported.
     *
     * @var string|NULL
     */
    public ?string $version = '1.0';

    /**
     * Returns value from property `meta` and initializes it if necessary.
     *
     * @return Meta
     */
    public function getMeta() : Meta
    {
        return $this->meta ?? ($this->meta = new Meta());
    }
}