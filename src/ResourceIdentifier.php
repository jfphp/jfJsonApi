<?php

namespace jf\JsonApi;

/**
 * A `resource identifier object` is an object that identifies an individual resource.
 *
 * @package jfJsonApi
 */
class ResourceIdentifier extends ANode
{
    /**
     * @inheritdoc
     */
    public const RULES = [
        'required' => [
            'id',
            'type'
        ]
    ];

    /**
     * Resource identifier.
     *
     * The id member is not required when the resource object originates at the client and
     * represents a new resource to be created on the server.
     *
     * @var string|NULL
     */
    public ?string $id = NULL;

    /**
     * A meta object containing non-standard meta-information about a resource that can not be
     * represented as an attribute or relationship.
     *
     * A `resource identifier object` MAY also include a meta member, whose value is a meta
     * object that contains non-standard meta-information.
     *
     * @var Meta|NULL
     */
    public ?Meta $meta = NULL;

    /**
     * The type member is used to describe resource objects that share common attributes and
     * relationships.
     *
     * @var string|NULL
     */
    public ?string $type = NULL;

    /**
     * Returns value from property `meta` and initializes it if necessary.
     *
     * @return Meta
     */
    public function getMeta() : Meta
    {
        return $this->meta ?? ($this->meta = new Meta());
    }
}