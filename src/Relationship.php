<?php

namespace jf\JsonApi;

/**
 * Represent references from the resource object in which it's defined to other resource
 * objects.
 *
 * @package jfJsonApi
 */
class Relationship extends ANode
{
    /**
     * Rules to validate value of instance.
     *
     * @inheritdoc
     */
    public const RULES = [
        'oneOf' => [
            'data',
            'links',
            'meta'
        ]
    ];

    /**
     * Allows a client to link together all of the included resource objects without having to
     * GET any URLs via links.
     *
     * @var ResourceIdentifier|NULL
     */
    public ?ResourceIdentifier $data = NULL;

    /**
     * A links object containing with information about relationship.
     *
     * @var Links|NULL
     */
    public ?Links $links = NULL;

    /**
     * Meta object containing non-standard meta-information about the relationship.
     *
     * @var Meta|NULL
     */
    public ?Meta $meta = NULL;

    /**
     * Returns value from property `data` and initializes it if necessary.
     *
     * @return ResourceIdentifier
     */
    public function getData() : ResourceIdentifier
    {
        return $this->data ?? ($this->data = new ResourceIdentifier());
    }

    /**
     * Returns value from property `links` and initializes it if necessary.
     *
     * @return Links
     */
    public function getLinks() : Links
    {
        return $this->links ?? ($this->links = new Links());
    }

    /**
     * Returns value from property `meta` and initializes it if necessary.
     *
     * @return Meta
     */
    public function getMeta() : Meta
    {
        return $this->meta ?? ($this->meta = new Meta());
    }
}