<?php

namespace jf\JsonApi;

/**
 * A links object containing error references.
 *
 * @package jfJsonApi
 */
class ErrorLink extends ANode
{
    /**
     * A link that leads to further details about this particular occurrence of the problem.
     *
     * @var string|NULL
     */
    public ?string $about = NULL;
}