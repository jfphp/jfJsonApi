<?php

namespace jf\JsonApi;

use Exception;
use JsonSerializable;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;

/**
 * Request and response manager.
 *
 * @package jfJsonApi
 */
class Manager
{
    /**
     * Options for JSON generation.
     *
     * @var int
     */
    public int $jsonOptions = JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;

    /**
     * URL query params.
     *
     * @var array
     */
    private array $_queryParams = [];

    /**
     * Request being validated.
     *
     * @var ServerRequestInterface|NULL
     */
    private ?ServerRequestInterface $_request = NULL;

    /**
     * Body of the request.
     *
     * @var object|NULL
     */
    private ?object $_requestBody = NULL;

    /**
     * Root node of JSON:API document.
     *
     * @var Root|NULL
     */
    private ?Root $_root = NULL;

    /**
     * Class constructor.
     *
     * @param ServerRequestInterface $request Request handler.
     */
    public function __construct(ServerRequestInterface $request)
    {
        $_root          = new Root();
        $this->_root    = $_root;
        $this->_request = $request;
        //
        $_root->getJsonApi();
        $_root->getLinks()->self = $request->getUri()->getPath();
    }

    /**
     * Configure response using root node.
     *
     * @param ResponseInterface $response Response to configure.
     *
     * @return ResponseInterface
     */
    public function configure(ResponseInterface $response) : ResponseInterface
    {
        try
        {
            $_root = $this->getRoot();
            $_body = $this->toJson($_root);
        }
        catch (Exception $e)
        {
            $_root = $this->_root = new Root();
            $_root->addError($e);
            $_body = $this->toJson($_root);
        }
        $response->getBody()?->write($_body);

        $_errors = $_root->getErrors();
        $_status = $_errors
            ? (int) $_errors[0]->status ?: 500
            : 200;

        return $response
            ->withHeader('Content-Type', Root::CONTENT_TYPE)
            ->withStatus($_status);
    }

    /**
     * Returns value from property `_queryParams`.
     *
     * @return array|NULL
     */
    public function getQueryParams() : ?array
    {
        return $this->_queryParams;
    }

    /**
     * Returns value from property `_request`.
     *
     * @return ServerRequestInterface|NULL
     */
    public function getRequest() : ?ServerRequestInterface
    {
        return $this->_request;
    }

    /**
     * Returns the body of the request.
     *
     * @return object|NULL
     */
    public function getRequestBody() : ?object
    {
        $_body = $this->_requestBody;
        if ($_body === NULL)
        {
            try
            {
                $_method = $this->getRequestMethod();
                if ($_method === 'PATCH' || $_method === 'POST')
                {
                    $this->_requestBody = $_body = json_decode((string) $this->_request->getBody()) ?? NULL;
                }
            }
            catch (Exception)
            {
            }
        }

        return $_body;
    }

    /**
     * Returns the HTTP method of the request.
     *
     * @return string
     */
    public function getRequestMethod() : string
    {
        return strtoupper($this->_request->getMethod());
    }

    /**
     * Builds a `Root` node with body of the request.
     *
     * @return Root|NULL
     */
    public function getRequestRoot() : ?Root
    {
        $_body = $this->getRequestBody();

        return $_body
            ? new Root(static::objectToArray($_body))
            : NULL;
    }

    /**
     * Returns value from property `_root` and initializes it if necessary.
     *
     * @return Root
     */
    public function getRoot() : Root
    {
        return $this->_root ?? ($this->_root = new Root());
    }

    /**
     * Process query params and return them as array.
     *
     * @throws UnexpectedValueException
     * @throws ValidationException
     */
    public function initQueryParams() : void
    {
        $_params = $this->_request->getQueryParams();
        if ($_params && empty($this->_queryParams))
        {
            /**
             * @var query\IQuery $_Class
             */
            foreach ($_params as $_name => $_value)
            {
                $_Class = sprintf('%s\query\%s', __NAMESPACE__, $_name === 'include' ? 'Includes' : ucfirst($_name));
                if (class_exists($_Class))
                {
                    Validator::assert($_Class::validate($_value), 400, 'Invalid query param format: {0}', $_name);
                    $this->_queryParams[ $_name ] = (new $_Class())->parse($_value);
                }
            }
        }
    }

    /**
     * Validates the client request.
     *
     * @return bool
     */
    public function isRequestValid() : bool
    {
        try
        {
            $this->initQueryParams();
            Validator::create($this->_request)->validate();
            $_isValid = TRUE;
        }
        catch (Exception $e)
        {
            $this->getRoot()->addError($e);
            $_isValid = FALSE;
        }

        return $_isValid;
    }

    /**
     * Serialize the data.
     *
     * @param array|object $data Datos a serializar.
     *
     * @return array
     */
    public static function objectToArray(array|object $data) : array
    {
        $_data = [];
        foreach ($data as $_key => $_value)
        {
            if ($_value instanceof JsonSerializable)
            {
                $_value = $_value->jsonSerialize();
            }
            elseif (is_array($_value) || is_object($_value))
            {
                $_value = static::objectToArray($_value);
            }
            $_data[ $_key ] = $_value;
        }
        ksort($_data);

        return $_data;
    }

    /**
     * Convert the data to JSON format.
     *
     * @param mixed $data Data to serialize.
     *
     * @return string
     */
    public function toJson(mixed $data = NULL) : string
    {
        return json_encode($data, $this->jsonOptions);
    }
}