<?php

namespace jf\JsonApi;

use Stringable;

/**
 * Base class for the rest of the classes in the project.
 *
 * @package jfJsonApi
 */
abstract class ABase implements Stringable
{
    /**
     * @inheritdoc
     */
    public function __toString() : string
    {
        return sprintf('[Class %s]', get_class($this));
    }

    /**
     * Create an instance of the class and initialize it.
     *
     * @param mixed $args Arguments to pass to the constructor of the class.
     *
     * @return static
     */
    public static function create(mixed ...$args) : static
    {
        return new static(...$args);
    }

    /**
     * Split a text using a char and remove blanks from begin and end of each line.
     *
     * @param string $value Text to convert.
     * @param string $char  Char to use to split the text.
     *
     * @return string[]|NULL
     */
    protected static function _explodeTrim(string $value, string $char = ',') : ?array
    {
        return array_filter(array_map('trim', explode($char, $value)));
    }

    /**
     * Returns name of the class without namespace.
     *
     * @return string
     */
    public static function getClassname() : string
    {
        $name = get_called_class();

        return substr($name, strrpos($name, '\\') + 1);
    }
}