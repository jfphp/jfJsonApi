<?php

namespace jf\JsonApi\query;

use jf\JsonApi\ABase;
use jf\JsonApi\ValidationException;
use jf\jsonApi\Validator;

/**
 * Construct the filter to use to restrict the result to the elements returned.
 *
 * The format is `filter[property]=operator|value`.
 *
 * Some examples:
 *
 * - filter[date]=eq|2017-07-20+18:00:00
 * - filter[name]=eq|joaquin
 * - filter[age]=between|1|5
 * - filter[amount]=lt|1234
 * - filter[statusId]=in|5|6|7
 * - filter[cityId]=123
 * - filter[period]=eq|week
 *
 * If the operator is not specified, `eq` is assumed. If the operator requires more than one
 * parameter, they are also separated by `|`.
 *
 * The available operators are:
 *
 * - eq: The value must be equal to the specified value.
 * - ge: The value must be greater than or equal to the specified value.
 * - gt: The value must be greater than the specified value.
 * - in: The value must be one of the specified values.
 * - le: The value must be less than or equal to the specified value.
 * - lt: The value must be less than the specified value.
 * - betw: The value must be between 2 values, both inclusive.
 * - like: The value must correspond to the specified LIKE expression.
 * - null: The value must be `null`.
 *
 * A `not` operation can be performed by prefixing the operator with `!`.
 *
 * @package jfJsonApi
 *
 * @see     http://jsonapi.org/format/#fetching-filtering
 */
class Filter extends ABase implements IQuery
{
    /**
     * Valid operators.
     *
     * @var array
     */
    public const OPERATORS = [
        'betw' => 2,
        'eq'   => 1,
        'ge'   => 1,
        'gt'   => 1,
        'in'   => -1,
        'le'   => 1,
        'like' => 1,
        'lt'   => 1,
        'ne'   => 1,
        'null' => 1
    ];

    /**
     * Fields separador.
     *
     * @var string
     */
    public const SEPARATOR = '|';

    /**
     * @throws ValidationException
     *
     * @inheritdoc
     */
    public function parse(array|string $data) : ?array
    {
        $_separator = self::SEPARATOR;
        $_where     = [];
        foreach ($data as $_attr => $_value)
        {
            $_value = explode($_separator, $_value, 2);
            if (count($_value) < 2)
            {
                array_unshift($_value, 'eq');
            }
            $_op  = trim($_value[0]);
            $_not = $_op[0] === '!';
            if ($_not)
            {
                $_op = substr($_op, 1);
            }
            Validator::assert(isset(self::OPERATORS[ $_op ]), 400, 'Unknown operator {0} for attribute {1}', $_op, $_attr);
            $_cmpValue = trim($_value[1]);
            $_count    = self::OPERATORS[ $_op ];
            if ($_count > 1)
            {
                $_cmpValue = explode($_separator, $_cmpValue, $_count);
                $_c        = count($_cmpValue);
                Validator::assert(
                    $_count === -1
                        ? $_c > 0
                        : $_c === $_count,
                    400,
                    'Incorrect number of arguments for the operator {0} for attribute {1}',
                    $_op,
                    $_attr
                );
            }
            $_where[] = [
                'attribute' => $_attr,
                'op'        => $_op,
                'not'       => $_not,
                'value'     => $_cmpValue
            ];
        }

        return $_where;
    }

    /**
     * @inheritdoc
     */
    public static function validate(mixed $data = NULL) : ?bool
    {
        return $data && is_array($data);
    }
}