<?php

namespace jf\JsonApi\query;

use jf\JsonApi\ABase;

/**
 * It is used to display the result of related resources. The format is
 * `include=Type1,Type2`.
 *
 * @note `Includes` is used since `Include` is a reserved word of PHP
 *
 * @package jfJsonApi
 *
 * @see http://jsonapi.org/format/#fetching-includes
 */
class Includes extends ABase implements IQuery
{
    /**
     * @inheritdoc
     */
    public function parse(array|string $data) : ?array
    {
        return is_string($data)
            ? $this->_explodeTrim($data)
            : array_filter(array_map('trim', $data));
    }

    /**
     * @inheritdoc
     */
    public static function validate(mixed $data = NULL) : ?bool
    {
        return $data && is_string($data);
    }
}