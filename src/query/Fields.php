<?php

namespace jf\JsonApi\query;

use jf\JsonApi\ABase;

/**
 * Allows you to select which attributes appear in the result.
 *
 * The format is `fields[table]=attribute1,attribute2`.
 *
 * @package jfJsonApi
 *
 * @see     http://jsonapi.org/format/#fetching-sparse-fieldsets
 */
class Fields extends ABase implements IQuery
{
    /**
     * @inheritdoc
     */
    public function parse(array|string $data) : ?array
    {
        return is_string($data)
            ? $this->_explodeTrim($data)
            : array_filter(array_map('trim', $data));
    }

    /**
     * @inheritdoc
     */
    public static function validate(mixed $data = NULL) : ?bool
    {
        return $data && (is_array($data) || is_string($data));
    }
}