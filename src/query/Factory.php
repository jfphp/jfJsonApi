<?php

namespace jf\JsonApi\query;

/**
 * Factory for handlers of query parameters.
 *
 * @package jfJsonApi
 */
class Factory
{
    /**
     * Parse params from an array of query params.
     *
     * @param array $params Query params to parse.
     *
     * @return array
     */
    public static function fromQueryParams(array $params) : array
    {
        if ($params)
        {
            foreach ($params as $_param => $_value)
            {
                $_class = sprintf(
                    '%s\%s',
                    __NAMESPACE__,
                    $_param === 'include'
                        ? 'Includes'
                        : ucfirst($_param)
                );
                if (class_exists($_class) && $_class::validate($_value))
                {
                    $params[$_param] = $_class::create()->parse($_value);
                }
            }
        }

        return $params;
    }

    /**
     * Parse params from query string.
     *
     * @param string $qs Query string to parse.
     *
     * @return array
     */
    public static function fromQueryString(string $qs) : array
    {
        parse_str($qs, $_params);

        return self::fromQueryParams($_params);
    }
}