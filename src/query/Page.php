<?php

namespace jf\JsonApi\query;

use jf\JsonApi\ABase;
use jf\JsonApi\ValidationException;
use jf\jsonApi\Validator;

/**
 * Allows you to paginate the list of results.
 *
 * The format is `page[number]=2&page[size]=50`.
 *
 * @package jfJsonApi
 *
 * @see http://jsonapi.org/format/#fetching-pagination
 */
class Page extends ABase implements IQuery
{
    /**
     * Maximum number of page.
     *
     * @var int
     */
    public static int $maxNumber = PHP_INT_MAX;

    /**
     * Maximum page size allowed.
     *
     * @var int
     */
    public static int $maxSize = 100;

    /**
     * Page size by default.
     *
     * @var int
     */
    public static int $size = 50;

    /**
     * @throws ValidationException
     *
     * @inheritdoc
     */
    public function parse(array|string $data) : ?array
    {
        $_data = [
            'number' => 1,
            'size'   => static::$size,
        ];
        foreach ($data as $_key => $_value)
        {
            Validator::assert(isset($_data[ $_key ]), 400, 'Unknown attribute {0} for query parameter `page`', $_key);
            Validator::assert(preg_match('/^\d+$/', $_value), 400, 'Attribute {0} for query parameter `page` must be an number greater than or equal to zero', $_key);
            $_data[ $_key ] = (int) $_value;
        }
        foreach ([ 'number' => [ 1, static::$maxNumber ], 'size' => [ 1, static::$maxSize ] ] as $_attribute => $_values)
        {
            Validator::assert($_data[ $_attribute ] >= $_values[0], 400, 'Attribute {0} for query parameter `page` must be greater than or equal to {1}', $_attribute, $_values[0]);
            Validator::assert($_data[ $_attribute ] <= $_values[1], 400, 'Attribute {0} for query parameter `page` must be less than or equal to {1}', $_attribute, $_values[1]);
        }

        return $_data;
    }

    /**
     * @inheritdoc
     */
    public static function validate(mixed $data = NULL) : ?bool
    {
        return $data && is_array($data);
    }
}