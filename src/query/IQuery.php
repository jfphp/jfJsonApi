<?php

namespace jf\JsonApi\query;

/**
 * Interface for query parsers.
 *
 * @package jfJsonApi
 */
interface IQuery
{
    /**
     * Parses the received data and processes it.
     *
     * @param array|string $data Received data from request.
     *
     * @return array|NULL
     */
    public function parse(array|string $data) : ?array;

    /**
     * Validates that the data can be processed by the class.
     *
     * @param mixed $data Data to validate.
     *
     * @return bool|NULL
     */
    public static function validate(mixed $data = NULL) : ?bool;
}