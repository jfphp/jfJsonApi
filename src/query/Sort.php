<?php

namespace jf\JsonApi\query;

use jf\JsonApi\ABase;

/**
 * Allows you to sort the results.
 *
 * The format is `sort=attribute1,-attribute2`.
 *
 * By default the order is ascending, but if the attribute is prefixed with `-` the order
 * will be descending.
 *
 * @package jfJsonApi
 *
 * @see http://jsonapi.org/format/#fetching-sorting
 */
class Sort extends ABase implements IQuery
{
    /**
     * @inheritdoc
     */
    public function parse(array|string $data) : ?array
    {
        $_attributes = [];
        foreach ($this->_explodeTrim($data) as $_attribute)
        {
            if ($_attribute[0] === '-')
            {
                $_attributes[ substr($_attribute, 1) ] = 'DESC';
            }
            elseif ($_attribute[0] === '+')
            {
                $_attributes[ substr($_attribute, 1) ] = 'ASC';
            }
            else
            {
                $_attributes[ $_attribute ] = 'ASC';
            }
        }

        return $_attributes;
    }

    /**
     * @inheritdoc
     */
    public static function validate(mixed $data = NULL) : ?bool
    {
        return $data && is_string($data);
    }
}