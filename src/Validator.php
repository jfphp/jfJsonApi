<?php

namespace jf\JsonApi;

use Exception;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;

/**
 * Validates the request.
 *
 * @package jfJsonApi
 */
class Validator extends ABase
{
    /**
     * Request methods allowed.
     *
     * @var array
     */
    public const ALLOWED_METHODS = [
        'DELETE',
        'GET',
        'HEAD',
        'OPTIONS',
        'PATCH',
        'POST'
    ];

    /**
     * Request being validated.
     *
     * @var ServerRequestInterface|NULL
     */
    protected ?ServerRequestInterface $_request = NULL;

    /**
     * Classes to throw depending of error code.
     *
     * @var Exception[]|ValidationException[]|string[]
     */
    public static array $throwables = [
        400 => ValidationException::class,
        405 => ValidationException::class,
        406 => ValidationException::class,
        415 => ValidationException::class,
        500 => ValidationException::class
    ];

    /**
     * Class constructor.
     *
     * @param ServerRequestInterface $request Request instance.
     */
    public function __construct(ServerRequestInterface $request)
    {
        $this->_request = $request;
    }

    /**
     * Throws an exception if condition is a falsy value.
     *
     * @param bool   $condition Condition to evaluate.
     * @param int    $code      Code for the exception.
     * @param string $message   Exception message.
     * @param mixed  $args      Arguments of `sprintf` function.
     *
     * @throws UnexpectedValueException
     * @throws ValidationException
     */
    public static function assert(bool $condition, int $code = 400, string $message = '', mixed ...$args)
    {
        if (!$condition)
        {
            if ($code < 400 || $code > 599)
            {
                $code = 400;
            }
            $_tr = [];
            foreach ($args as $_index => $_arg)
            {
                if (!is_scalar($_arg))
                {
                    $_arg = json_encode($_arg);
                }
                $_tr[ '{' . $_index . '}' ] = '`' . $_arg . '`';
            }
            $_Class = static::$throwables[ $code ] ?? UnexpectedValueException::class;

            throw new $_Class(strtr($message, $_tr), $code);
        }
    }

    /**
     * Check if request has empty body.
     *
     * @throws ValidationException
     */
    private function _assertEmptyBody()
    {
        $_request = $this->_request;
        self::assert(
            empty((string) $_request->getBody()),
            400,
            'Method {0} don\'t accept data in request body',
            $_request->getMethod()
        );
    }

    /**
     * Check if request has not query parameters.
     *
     * @throws ValidationException
     */
    private function _assertEmptyQuery()
    {
        $_request = $this->_request;
        self::assert(
            empty($_request->getUri()->getQuery()),
            400,
            'Method {0} don\'t accept query parameters',
            $_request->getMethod()
        );
    }

    /**
     * Check value of header.
     *
     * @param string $header Header name.
     * @param int    $code   Error code.
     *
     * @throws ValidationException
     */
    private function _assertHeader(string $header, int $code)
    {
        $_header = $this->_explodeTrim(strtolower($this->_request->getHeaderLine($header)));
        self::assert((bool) $_header, $code, 'Empty header {0}', $header);
        self::assert(count($_header) === 1, $code, 'Wrong header -- {0}', $header . ': ' . $_header[0]);

        $_header = $_header[0];
        self::assert($_header === Root::CONTENT_TYPE, $code, 'Wrong header -- {0}', $header . ': ' . $_header);
    }

    /**
     * Check if request is valid.
     *
     * @throws ValidationException
     */
    public function validate()
    {
        // Servers MUST respond with a 406 Not Acceptable status code if a request’s Accept header contains
        // the JSON API media type and all instances of that media type are modified with media type parameters.
        $this->_assertHeader('Accept', 406);

        // Servers MUST respond with a 415 Unsupported Media Type status code if a request specifies the
        // header Content-Type: application/vnd.api+json with any media type parameters.
        $this->_assertHeader('Content-Type', 415);

        $_method = strtoupper($this->_request->getMethod());
        self::assert(in_array($_method, static::ALLOWED_METHODS), 405, 'Method {0} not allowed', $_method);
        switch ($_method)
        {
            case 'DELETE':
                $this->_assertEmptyQuery();
                $this->_assertEmptyBody();
                break;
            case 'PATCH':
                $this->_assertEmptyQuery();
                static::validateProperty($this->_validateBody()->data, 'id', 'is_string', 'data.');
                break;
            case 'POST':
                $this->_assertEmptyQuery();
                static::assert(!property_exists($this->_validateBody()->data, 'id'), 400, 'Unexpectd attribute `data.id`');
                break;
            default:
                $this->_assertEmptyBody();
                break;
        }
    }

    /**
     * Check request body.
     *
     * @return object|NULL Body object after validations.
     *
     * @throws ValidationException
     */
    private function _validateBody() : ?object
    {
        $_body = json_decode((string) $this->_request->getBody()) ?? NULL;

        static::assert(is_object($_body), 400, 'Wrong body format');

        // Expected keys and types
        static::validateProperty($_body, 'data', 'is_object');
        $_data = $_body->data;
        static::validateProperty($_data, 'type', 'is_string', 'data.');
        if (property_exists($_data, 'attributes'))
        {
            static::validateProperty($_data, 'attributes', 'is_object', 'data.');
        }

        // Check not allowed keys in data object.
        $_keys = array_diff(array_keys((array) $_data), [ 'attributes', 'id', 'relationships', 'type' ]);
        static::assert(empty($_keys), 400, 'Invalid key(s) in `data`: {0}', array_values($_keys));

        return $_body;
    }

    /**
     * Check if a property in object is valid.
     *
     * @param object   $object   Object with property to validate.
     * @param string   $property Name of the property to validate.
     * @param callable $callable PHP function to use to validate datatype.
     * @param string   $prefix   Prefix to display on error.
     *
     * @throws ValidationException
     */
    public static function validateProperty(object $object, string $property, callable $callable, string $prefix = '')
    {
        static::assert(property_exists($object, $property), 400, 'Key {0} is required', $prefix . $property);
        $_value = $object->$property;
        static::assert($callable($_value), 400, '{0} must be of type {1}', $prefix . $property, substr($callable, 3));
        if ($callable === 'is_object')
        {
            $_value = (array) $_value;
        }
        static::assert(!empty($_value), 400, '{0} can\'t be empty', $prefix . $property);
    }
}