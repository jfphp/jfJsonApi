<?php

namespace jf\JsonApi;

/**
 * Each member of a links object is a `link`. A link MUST be represented as either:
 *
 * - A string containing the link's URL.
 * - An object (`link object`) which can contain the following members:
 *   - href: A string containing the link's URL.
 *   - meta: A meta object containing non-standard meta-information about the link.
 *
 * @package jfJsonApi
 */
class Link extends ANode
{
    /**
     * Rules to validate value of instance.
     *
     * @inheritdoc
     */
    public const RULES = [
        'required' => [
            'href'
        ]
    ];

    /**
     * String containing the link's URL.
     *
     * @var string|NULL
     */
    public ?string $href = NULL;

    /**
     * Meta object containing non-standard meta-information about the link.
     *
     * @var Meta|NULL
     */
    public ?Meta $meta = NULL;

    /**
     * Returns value from property `meta` and initializes it if necessary.
     *
     * @return Meta
     */
    public function getMeta() : Meta
    {
        return $this->meta ?? ($this->meta = new Meta());
    }
}